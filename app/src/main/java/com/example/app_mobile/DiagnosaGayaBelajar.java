package com.example.app_mobile;

import static com.example.app_mobile.HasilKepribadian.KEPRIBADIAN;
import static com.example.app_mobile.HasilKepribadian.NAME;
import static com.example.app_mobile.HasilKepribadian.PRESENTASE_TERBESAR;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagnosaGayaBelajar extends AppCompatActivity implements View.OnClickListener {

    private static final String JSON_URL = Server.URL + "CiriGayabelajar";
    private static final String URL_KNOWLEGBASE = Server.URL + "BasisPengetahuan";

    Button btn_hitung;
    RecyclerView recyclerView;
    LearningstyleAdapter adapter;
    private String kepribadian;
    private double presentase_kepribadian;

    private List<CiriGayabelajarItem> Item_Diagnosa;
    private List<BasisPengetahuan> Item_BasisPengetahuan;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosa_gaya_belajar);
        btn_hitung = (Button) findViewById(R.id.btn_hitung);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        kepribadian = getIntent().getStringExtra(KEPRIBADIAN);
        presentase_kepribadian = getIntent().getDoubleExtra(PRESENTASE_TERBESAR, 0.0);
        name = getIntent().getStringExtra(NAME);

        btn_hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                basispengetahuan();
                for (CiriGayabelajarItem item : Item_Diagnosa) {
                    Log.d("cfuser", String.valueOf(item.getCfuser()));
                }
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Item_Diagnosa = new ArrayList<>();
        gayabelajar();
    }

    private void gayabelajar() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                response -> {

                    Log.d(DiagnosaGayaBelajar.class.getSimpleName(), "responnya" + response.toString());
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONArray playerArray = obj.getJSONArray("gb");
                        Item_BasisPengetahuan = new ArrayList<>();
                        for (int i = 0; i < playerArray.length(); i++) {

                            JSONObject gayabelajarObject = playerArray.getJSONObject(i);


                            CiriGayabelajarItem lsItem = new CiriGayabelajarItem();
                            lsItem.setKode_gb(gayabelajarObject.getString("kode_gb"));
                            lsItem.setCiri_gb(gayabelajarObject.getString("ciri_gb"));
                            lsItem.setCfuser(0.0);
                            Item_Diagnosa.add(lsItem);
                        }

                        LearningstyleAdapter adapter = new LearningstyleAdapter(Item_Diagnosa, getApplicationContext());
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Toast.makeText(getApplicationContext(), "ERROR 2 " + error.getMessage(), Toast.LENGTH_SHORT).show());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void basispengetahuan() {
        String[] kodeVisual = {"L01", "L02", "L04", "L07", "L12"};
        String[] kodeAuditorial = {"L02", "L05", "L08", "L09", "L10", "L11", "L12"};
        String[] kodeKinestetik = {"L03", "L06", "L12", "L13", "L14", "L15", "L16", "L17", "L18"};

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_KNOWLEGBASE,
                response -> {

                    Log.d(DiagnosaGayaBelajar.class.getSimpleName(), "responnya" + response.toString());
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONArray playerArray = obj.getJSONArray("bd");

                        for (int i = 0; i < playerArray.length(); i++) {

                            JSONObject gayabelajarObject = playerArray.getJSONObject(i);

                            BasisPengetahuan bdItem = new BasisPengetahuan();
                            bdItem.setKode_ciri(gayabelajarObject.getString("kode_ciri"));
                            bdItem.setCf(gayabelajarObject.getDouble("cf"));

                            Item_BasisPengetahuan.add(bdItem);
                        }
                        //split array ciri per learningstyle
                        ArrayList<CiriGayabelajarItem> ciriVisual = new ArrayList<>();
                        ArrayList<CiriGayabelajarItem> ciriAuditorial = new ArrayList<>();
                        ArrayList<CiriGayabelajarItem> ciriKinestetik = new ArrayList<>();

                        for (CiriGayabelajarItem item : Item_Diagnosa) {
                            if (Arrays.asList(kodeVisual).contains(item.getKode_gb())) {
                                ciriVisual.add(item);
                            } else if (Arrays.asList(kodeAuditorial).contains(item.getKode_gb())) {
                                ciriAuditorial.add(item);
                            } else if (Arrays.asList(kodeKinestetik).contains(item.getKode_gb())) {
                                ciriKinestetik.add(item);
                            }
                        }

                        double resultVisual = hitungResult(ciriVisual);
                        Log.d("resultVisual", String.valueOf(resultVisual));
                        double resultAuditorial = hitungResult(ciriAuditorial);
                        Log.d("resultAuditorial", String.valueOf(resultAuditorial));
                        double resultKinestetik = hitungResult(ciriKinestetik);
                        Log.d("resultKinestetik", String.valueOf(resultKinestetik));

                        if (resultVisual == 0.0 && resultAuditorial == 0.0 && resultKinestetik == 0.0) {
                            Toast.makeText(DiagnosaGayaBelajar.this, "Isi diagnosa terlebih dahulu!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Intent intent = new Intent(DiagnosaGayaBelajar.this, HasilGayabelajar.class);
                        intent.putExtra(HasilGayabelajar.PRESENTASE_VISUAL, resultVisual);
                        intent.putExtra(HasilGayabelajar.PRESENTASE_AUDITORIAL, resultAuditorial);
                        intent.putExtra(HasilGayabelajar.PRESENTASE_KINESTETIK, resultKinestetik);
                        intent.putExtra(KEPRIBADIAN, kepribadian);
                        intent.putExtra(PRESENTASE_TERBESAR, presentase_kepribadian);
                        intent.putExtra(NAME, name);
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Toast.makeText(getApplicationContext(), "ERROR 2 " + error.getMessage(), Toast.LENGTH_SHORT).show());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private double hitungResult(List<CiriGayabelajarItem> listCiri) {
        double firstCfhe = getCfhe(listCiri.get(0));

        Log.i("FirstCFHE", String.valueOf(firstCfhe));

        return hitungDiagnosa(firstCfhe, 1, listCiri);
    }

    private double hitungDiagnosa(double cfhe, int i, List<CiriGayabelajarItem> gejala) {

//        cek apakah sudah index akhir, jika sudah maka return hasil dalam satuan persen, jika tidak lanjut hitung
        if (i < gejala.size()) {

//            get cfhe ciri ke i
            double newCfhe = getCfhe(gejala.get(i));

//            hitung kombinasi
            double cfcombine = cfhe + newCfhe * (1 - cfhe);
            i++;

//            hitung combinasi berikunya hingga akhir index
            return hitungDiagnosa(cfcombine, i, gejala);
        }

        Log.i("CFHE", String.valueOf(cfhe));
        return cfhe * 100;
    }

    private double getCfhe(CiriGayabelajarItem ciri) {
        for (BasisPengetahuan item : Item_BasisPengetahuan) {
            if (item.kode_ciri.equals(ciri.getKode_gb())) {
                Log.i("CFUser", String.valueOf(ciri.getCfuser()));
                return ciri.getCfuser() * item.getCf();
            }
        }
        return 1;
    }

    @Override
    public void onClick(View view) {

    }

}