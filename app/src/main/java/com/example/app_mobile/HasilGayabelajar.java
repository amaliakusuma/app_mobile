package com.example.app_mobile;

import static com.example.app_mobile.HasilKepribadian.KEPRIBADIAN;
import static com.example.app_mobile.HasilKepribadian.NAME;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public class HasilGayabelajar extends AppCompatActivity {

    public static final String GAYABELAJAR = "gayabelajar";
    public static final String PRESENTASE_TERBESAR = "presentase_terbesar";
    public static final String PRESENTASE_VISUAL = "presentase_visual";
    public static final String PRESENTASE_AUDITORIAL = "presentase_auditorial";
    public static final String PRESENTASE_KINESTETIK = "presentase_kinestetik";
    String kepribadian;
    double persentase_kepribadian;
    String gaya_belajar;
    double persentase_gaya_belajar;

    //firebase
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser user;

    Button btn_hasilakhir;
    TextView txt_diagnosa, txt_presentase, txt_deskripsi, txt_saran;

    double visuals, auditorials, kinestetiks, max_presentase;
    int index_gayabelajar = 0;
    private String name;

    String gb[] = {
            "VISUAL",
            "AUDITORIAL",
            "KINESTETIK"
    };
    String deskripsi_GB[] = {
            "Gaya belajar visual berfokus pada penglihatan. Saat mempelajari hal baru, biasanya tipe ini perlu melihat sesuatu secara visual untuk lebih mudah mengerti dan memahami. Selain itu, tipe visual juga lebih nyaman belajar dengan pengunaan warna-warna, garis, maupun bentuk.",
            "Gaya belajar auditori, biasanya kamu lebih mengandalkan pendengaran untuk menerima informasi dan pengetahuan. Orang tipe auditori tidak masalah dengan tampilan visual saat mengajar, yang penting adalah mendengarkan pembicaraan guru dengan baik dan jelas. Nah, makanya tipe auditori biasanya paling peka dan hafal dari setiap ucapan yang pernah didengar bukan apa yang dilihat.",
            "Gaya belajar ini menyenangi belajar yang melibatkan gerakan. Biasanya orang yang tipe ini, merasa lebih mudah mempelajari sesuatu tidak hanya sekadar membaca buku tetapi juga mempraktikkanya. Dengan melakukan atau menyentuh objek yang dipelajari akan memberikan pengalaman tersendiri bagi tipe kinestetik. Makanya, orang yang memiliki gaya belajar tipe kinestetik biasanya tidak betah berdiam lama-lama di kelas "
    };
    String saran_GB[] = {
            "Gunakanlah Variasi warna dalam melakukan pencatatan, seperti memberi garis bawah atau membuat grafik. Mayoritas, tipe visual suka membaca. Namun, buku bacaan yang banyak memiliki gambar ilustrasi dan warna yang menarik lebih mudah dipahami daripada buku bacaan yang penuh dengan teks. Perhatikan penerangan saat belajar dan hindari “polusi visual.” Saat mengingat sesuatu, bayangkan dan buat tulisan yang memudahkan. Catat kembali bahan pelajaran dengan warna dan gambar yang menarik.",
            "Gunakanlah voice recorder atau perekam suara saat mendengarkan pelajaran. Perbanyak melakukan presentasi dan tanya jawab. Lagukan apa yang diingat dengan irama dan hindari “polusi suara” (kebisingan). Berpikir dan mengingat sambil mengucapkannya kembali. Dengarkan kembali pelajaran melalui rekaman atau penjelasan orang lain",
            "Gunakanlah gerakan dalam pelajaran, seperti aktivitas atau uji coba secara langsung. Perbanyak praktik yang berkaitan dengan pelajaran (praktik di laboratorium) dan langsung bisa diaplikasikan. Hindari belajar yang monoton (terlalu banyak duduk). Saat mengingat sesuatu, lakukan hal yang diingat dengan aktivitas gerak. Menulis di udara, gunakan gerak imajitif."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_gayabelajar);

        // firebase
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        db = FirebaseFirestore.getInstance();

        visuals = getIntent().getDoubleExtra(PRESENTASE_VISUAL, 0.0);
        auditorials = getIntent().getDoubleExtra(PRESENTASE_AUDITORIAL, 0.0);
        kinestetiks = getIntent().getDoubleExtra(PRESENTASE_KINESTETIK, 0.0);
        name = getIntent().getStringExtra(NAME);

        btn_hasilakhir = (Button) findViewById(R.id.btn_hasilakhir);
        txt_diagnosa = findViewById(R.id.diagnosa);
        txt_presentase = findViewById(R.id.presentase);
        txt_deskripsi = findViewById(R.id.deskripsi);
        txt_saran = findViewById(R.id.saran);

        btn_hasilakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HasilGayabelajar.this, HistoryDiagnosa.class);
                startActivity(intent);
            }

        });
        double max = visuals;
        if (max < auditorials) {
            max = auditorials;
            index_gayabelajar = 1;
        }
        if (max < kinestetiks) {
            max = kinestetiks;
            index_gayabelajar = 2;
        }
        txt_diagnosa.setText(gb[index_gayabelajar]);
        txt_presentase.setText(String.format("%.2f", max));
        txt_deskripsi.setText(deskripsi_GB[index_gayabelajar]);
        txt_saran.setText(saran_GB[index_gayabelajar]);

        kepribadian = getIntent().getStringExtra(KEPRIBADIAN);
        persentase_kepribadian = getIntent().getDoubleExtra(PRESENTASE_TERBESAR, 0.0);
        gaya_belajar = gb[index_gayabelajar];
        persentase_gaya_belajar = max;

        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", user.getUid());
        data.put("kepribadian", kepribadian);
        data.put("persentaseKepribadian", persentase_kepribadian);
        data.put("gayaBelajar", gaya_belajar);
        data.put("persentaseGayaBelajar", persentase_gaya_belajar);
        data.put("tanggalDiagnosa", Timestamp.now());
        data.put("nama", name);

        db.collection("diagnosa").add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(HasilGayabelajar.this, "Berhasil menyimpan", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(HasilGayabelajar.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}