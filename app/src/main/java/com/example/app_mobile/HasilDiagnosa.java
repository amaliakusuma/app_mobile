package com.example.app_mobile;

import com.google.firebase.Timestamp;

public class HasilDiagnosa {
    private String id;
    private String nama;
    private String userId;
    private String kepribadian;
    private double persentaseKepribadian;
    private String gayaBelajar;
    private double persentaseGayaBelajar;
    private Timestamp tanggalDiagnosa;

    public HasilDiagnosa() {
    }

    public HasilDiagnosa(String id, String nama, String userId, String kepribadian, double persentaseKepribadian, String gayaBelajar, double persentaseGayaBelajar, Timestamp tanggalDiagnosa) {
        this.id = id;
        this.nama = nama;
        this.userId = userId;
        this.kepribadian = kepribadian;
        this.persentaseKepribadian = persentaseKepribadian;
        this.gayaBelajar = gayaBelajar;
        this.persentaseGayaBelajar = persentaseGayaBelajar;
        this.tanggalDiagnosa = tanggalDiagnosa;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getKepribadian() {
        return kepribadian;
    }

    public void setKepribadian(String kepribadian) {
        this.kepribadian = kepribadian;
    }

    public double getPersentaseKepribadian() {
        return persentaseKepribadian;
    }

    public void setPersentaseKepribadian(double persentaseKepribadian) {
        this.persentaseKepribadian = persentaseKepribadian;
    }

    public String getGayaBelajar() {
        return gayaBelajar;
    }

    public void setGayaBelajar(String gayaBelajar) {
        this.gayaBelajar = gayaBelajar;
    }

    public double getPersentaseGayaBelajar() {
        return persentaseGayaBelajar;
    }

    public void setPersentaseGayaBelajar(double persentaseGayaBelajar) {
        this.persentaseGayaBelajar = persentaseGayaBelajar;
    }

    public Timestamp getTanggalDiagnosa() {
        return tanggalDiagnosa;
    }

    public void setTanggalDiagnosa(Timestamp tanggalDiagnosa) {
        this.tanggalDiagnosa = tanggalDiagnosa;
    }
}
