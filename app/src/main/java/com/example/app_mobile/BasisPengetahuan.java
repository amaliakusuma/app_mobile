package com.example.app_mobile;

import java.io.Serializable;

public class BasisPengetahuan implements Serializable {

    Integer id;
    String kode_ciri;
    public double mb;
    public double md;
    public double cf;
    public double cfpakar;

    public BasisPengetahuan() {

    }

    public BasisPengetahuan(Integer id, String kode_ciri, double mb, double md, double cf, double cfpakar) {
        this.id = id;
        this.kode_ciri = kode_ciri;
        this.mb = mb;
        this.md = md;
        this.cf = cf;
        this.cfpakar = cfpakar;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKode_ciri() {
        return this.kode_ciri;
    }

    public void setKode_ciri(String kode_ciri) {
        this.kode_ciri = kode_ciri;
    }

    public double getMb() {
        return this.mb;
    }

    public void setMb(double mb) {
        this.mb = mb;
    }

    public double getMd() {
        return this.md;
    }

    public void setMd(double md) {
        this.md = md;
    }

    public double getCf() {
        return this.cf;
    }

    public void setCf(double cf) {
        this.cf = cf;
    }

    public double getCfpakar() {
        return this.cfpakar;
    }

    public void setCfpakar(double cfpakar) {
        this.cfpakar = cfpakar;
    }
}
