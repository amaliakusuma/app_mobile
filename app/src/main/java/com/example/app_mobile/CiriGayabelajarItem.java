package com.example.app_mobile;

import java.io.Serializable;

public class CiriGayabelajarItem implements Serializable {
    private String kode_gb;
    private String ciri_gb;
    private double cfuser;

    public CiriGayabelajarItem() {

    }

    public CiriGayabelajarItem(String kode_gb, String ciri_gb, double cfuser) {
        this.kode_gb = kode_gb;
        this.ciri_gb = ciri_gb;
        this.cfuser = cfuser;
    }

    public String getKode_gb() {
        return kode_gb;
    }

    public void setKode_gb(String kode_gb) {
        this.kode_gb = kode_gb;
    }

    public String getCiri_gb() {
        return ciri_gb;
    }

    public void setCiri_gb(String ciri_gb) {
        this.ciri_gb = ciri_gb;
    }

    public double getCfuser() {
        return cfuser;
        //return getCfuser();
    }

    public void setCfuser(double cfuser) {
        this.cfuser = cfuser;
    }

    @Override
    public String toString() {
        return "CiriGayabelajarItem{" +
                "kode_gb='" + kode_gb + '\'' +
                ", ciri_gb='" + ciri_gb + '\'' +
                ", cfuser=" + cfuser +
                '}';
    }

}
