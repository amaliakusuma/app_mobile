package com.example.app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagnosaKepribadian extends AppCompatActivity implements View.OnClickListener {

    private static final String JSON_URL = Server.URL + "CiriKepribadian";
    private static final String URL_KNOWLEGBASE = Server.URL + "BasisPengetahuan";
    Button btn_hitung;
    RecyclerView recyclerView;
    PersonalityAdapter adapter;
    private List<CiriKepribadianItem> Item_Diagnosa;
    private List<BasisPengetahuan> Item_BasisPengetahuan;
    private String name;
    private TextInputEditText inputName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosa_kepribadian);
        btn_hitung = (Button) findViewById(R.id.btn_hitung);
        inputName = findViewById(R.id.inputName);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_hitung.setOnClickListener(view -> {
            if (inputName.getText() == null || inputName.getText().toString().isEmpty()) {
                Toast.makeText(DiagnosaKepribadian.this, "Nama tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                return;
            }

            name = inputName.getText().toString();
            basispengetahuan();
            for (CiriKepribadianItem item : Item_Diagnosa) {
                Log.d("cfuser", String.valueOf(item.getCfuser()));
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Item_Diagnosa = new ArrayList<>();
        kepribadian();
    }

    private void kepribadian() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                response -> {

                    Log.d(DiagnosaKepribadian.class.getSimpleName(), "responnya" + response.toString());
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONArray playerArray = obj.getJSONArray("ck");

                        for (int i = 0; i < playerArray.length(); i++) {
                            JSONObject kepribadianObject = playerArray.getJSONObject(i);

                            CiriKepribadianItem ckItem = new CiriKepribadianItem();
                            ckItem.setKode_kepribadian(kepribadianObject.getString("kode_kepribadian"));
                            ckItem.setCiri_kepribadian(kepribadianObject.getString("ciri_kepribadian"));
                            ckItem.setCfuser(0.0);
                            Item_Diagnosa.add(ckItem);
                        }

                        PersonalityAdapter adapter = new PersonalityAdapter(Item_Diagnosa, getApplicationContext());
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Toast.makeText(getApplicationContext(), "ERROR 2 " + error.getMessage(), Toast.LENGTH_SHORT).show());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void basispengetahuan() {
        String[] kodeSanguinis = {"P04", "P07", "P10", "P12", "P13", "P16"};
        String[] kodeMelankolis = {"P01", "P09", "P17", "P18", "P20", "P21", "P23", "P24", "P25"};
        String[] kodeKorelis = {"P02", "P03", "P05", "P08", "P09", "P14", "P19", "P22"};
        String[] kodePlegmatis = {"P11", "P15"};

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_KNOWLEGBASE,
                response -> {
                    Log.d(DiagnosaKepribadian.class.getSimpleName(), "responnya" + response.toString());
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONArray playerArray = obj.getJSONArray("bd");
                        Item_BasisPengetahuan = new ArrayList<>();
                        for (int i = 0; i < playerArray.length(); i++) {
                            JSONObject kepribadianObject = playerArray.getJSONObject(i);

                            BasisPengetahuan bdItem = new BasisPengetahuan();
                            bdItem.setKode_ciri(kepribadianObject.getString("kode_ciri"));
                            bdItem.setCf(kepribadianObject.getDouble("cf"));

                            Item_BasisPengetahuan.add(bdItem);
                        }

                        //split array ciri per personality
                        ArrayList<CiriKepribadianItem> ciriSanguinis = new ArrayList<>();
                        ArrayList<CiriKepribadianItem> ciriMelankolis = new ArrayList<>();
                        ArrayList<CiriKepribadianItem> ciriKorelis = new ArrayList<>();
                        ArrayList<CiriKepribadianItem> ciriPlegmatis = new ArrayList<>();

                        for (CiriKepribadianItem item : Item_Diagnosa) {
                            if (Arrays.asList(kodeSanguinis).contains(item.getKode_kepribadian())) {
                                ciriSanguinis.add(item);
                            } else if (Arrays.asList(kodeMelankolis).contains(item.getKode_kepribadian())) {
                                ciriMelankolis.add(item);
                            } else if (Arrays.asList(kodeKorelis).contains(item.getKode_kepribadian())) {
                                ciriKorelis.add(item);
                            } else if (Arrays.asList(kodePlegmatis).contains(item.getKode_kepribadian())) {
                                ciriPlegmatis.add(item);
                            }
                        }

                        double resultSanguinis = hitungResult(ciriSanguinis);
                        Log.d("resultSanguinis", String.valueOf(resultSanguinis));
                        double resultMelankolis = hitungResult(ciriMelankolis);
                        Log.d("resultMelankolis", String.valueOf(resultMelankolis));
                        double resultKorelis = hitungResult(ciriKorelis);
                        Log.d("resultKorelis", String.valueOf(resultKorelis));
                        double resultPlegmatis = hitungResult(ciriPlegmatis);
                        Log.d("resultPlegmatis", String.valueOf(resultPlegmatis));

                        if (resultSanguinis == 0.0 && resultMelankolis == 0.0 && resultKorelis == 0.0 && resultPlegmatis == 0.0) {
                            Toast.makeText(DiagnosaKepribadian.this, "Isi diagnosa terlebih dahulu!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Intent intent = new Intent(DiagnosaKepribadian.this, HasilKepribadian.class);
                        intent.putExtra(HasilKepribadian.PRESENTASE_SANGUINIS, resultSanguinis);
                        intent.putExtra(HasilKepribadian.PRESENTASE_MELANKOLIS, resultMelankolis);
                        intent.putExtra(HasilKepribadian.PRESENTASE_KORELIS, resultKorelis);
                        intent.putExtra(HasilKepribadian.PRESENTASE_PLEGMATIS, resultPlegmatis);
                        intent.putExtra(HasilKepribadian.NAME, name);
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Toast.makeText(getApplicationContext(), "ERROR 2 " + error.getMessage(), Toast.LENGTH_SHORT).show());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private double hitungResult(List<CiriKepribadianItem> listCiri) {
        double firstCfhe = getCfhe(listCiri.get(0));

        Log.i("FirstCFHE", String.valueOf(firstCfhe));

        return hitungDiagnosa(firstCfhe, 1, listCiri);
    }

    private double hitungDiagnosa(double cfhe, int i, List<CiriKepribadianItem> gejala) {

//        cek apakah sudah index akhir, jika sudah maka return hasil dalam satuan persen, jika tidak lanjut hitung
        if (i < gejala.size()) {

//            get cfhe ciri ke i
            double newCfhe = getCfhe(gejala.get(i));

//            hitung kombinasi
            double cfcombine = cfhe + newCfhe * (1 - cfhe);
            i++;

//            hitung combinasi berikunya hingga akhir index
            return hitungDiagnosa(cfcombine, i, gejala);
        }

        Log.i("CFHE", String.valueOf(cfhe));
        return cfhe * 100;
    }

    private double getCfhe(CiriKepribadianItem ciri) {
        for (BasisPengetahuan item : Item_BasisPengetahuan) {
            if (item.kode_ciri.equals(ciri.getKode_kepribadian())) {
                Log.i("CFUser", String.valueOf(ciri.getCfuser()));
                return ciri.getCfuser() * item.getCf();
            }
        }
        return 0;
    }

    @Override
    public void onClick(View view) {

    }
}