package com.example.app_mobile;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Handler;import android.os.Bundle;

public class splash_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final Handler handler = new Handler();
        handler.postDelayed (() -> {
            startActivity(new Intent(getApplicationContext(), Login.class));
            finish();
        }, 3000L);
    }
}
