package com.example.app_mobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LearningstyleAdapter extends RecyclerView.Adapter<LearningstyleAdapter.ViewHolder> {

    public List<CiriGayabelajarItem> Item_Diagnosa;
    HashMap<Integer,Integer>selectedspinners=new HashMap<>();
    //private Context context;
    //private android.widget.Spinner spinnerText;


    public LearningstyleAdapter(List<CiriGayabelajarItem> Item_Diagnosa, Context context) {
        //super(context, R.layout.activity_item_diagnosa, Item_Diagnosa);
        this.Item_Diagnosa = Item_Diagnosa;
        //this.context = context;
    }

    @NonNull
    @Override
    public LearningstyleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_item_diagnosa, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getItemCount() {

        return Item_Diagnosa.size();

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public void onBindViewHolder(@NonNull LearningstyleAdapter.ViewHolder holder, int position) {
        holder.bind(Item_Diagnosa.get(position), position);
        //holder.gejalaList.setText(Item_Diagnosa.get(position).getCiri());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView gejalaList;
        Spinner spinnerText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            gejalaList = itemView.findViewById(R.id.gejalaList);
            spinnerText = itemView.findViewById(R.id.spinnerText);
        }


        public void bind(final CiriGayabelajarItem lsItem, int pos) {

            gejalaList.setText(lsItem.getCiri_gb());
            final ArrayList<String> options = new ArrayList<>();
            options.add("Tidak");
            options.add("Tidak Tahu");
            options.add("Sedikit Yakin");
            options.add("Cukup Yakin");
            options.add("Yakin");
            options.add("Sangat Yakin");

            ArrayAdapter adapter = new ArrayAdapter(itemView.getContext(),
                    android.R.layout.simple_spinner_dropdown_item, options);

            spinnerText.setAdapter(adapter);
            if (selectedspinners.get(pos)!=null&&selectedspinners.get(pos)!=-1){
                spinnerText.setSelection(selectedspinners.get(pos));
            }

            spinnerText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedspinners.put(pos,position);
                    if ("Tidak".equals(options.get(position))) {
                        lsItem.setCfuser(0);
                    } else if ("Tidak Tahu".equals(options.get(position))) {
                        lsItem.setCfuser(0.2);
                    } else if ("Sedikit Yakin".equals(options.get(position))) {
                        lsItem.setCfuser(0.4);
                    } else if ("Cukup Yakin".equals(options.get(position))) {
                        lsItem.setCfuser(0.6);
                    } else if ("Yakin".equals(options.get(position))) {
                        lsItem.setCfuser(0.8);
                    } else if ("Sangat Yakin".equals(options.get(position))) {
                        lsItem.setCfuser(1);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }
}


//public class LearningstyleAdapter extends RecyclerView.Adapter<LearningstyleAdapter.ViewHolder> {
//
//    public List<CiriGayabelajarItem> Item_Diagnosa;
//    HashMap<Integer,Integer>selectedspinners=new HashMap<>();
//
//
//
//    public LearningstyleAdapter(List<CiriGayabelajarItem> Item_Diagnosa, Context context) {
//
//        this.Item_Diagnosa = Item_Diagnosa;
//    }
//
//    @NonNull
//    @Override
//    public LearningstyleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.activity_item_diagnosa, parent, false);
//
//        ViewHolder viewHolder = new ViewHolder(v);
//        return viewHolder;
//    }
//
//    @Override
//    public int getItemCount() {
//        return Item_Diagnosa.size();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//
//    @Override
//    public void onBindViewHolder(@NonNull LearningstyleAdapter.ViewHolder holder, int position) {
//        holder.bind(Item_Diagnosa.get(position), position);
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        TextView gejalaList;
//        View spinnerText;
//
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//            gejalaList = itemView.findViewById(R.id.gejalaList);
//            spinnerText = itemView.findViewById(R.id.spinnerText);
//
//        }
//
//        public void bind(final CiriGayabelajarItem lsItem, int pos) {
//            gejalaList.setText(lsItem.getCiri_ls());
//            final ArrayList<String> options = new ArrayList<>();
//            options.add("Tidak");
//            options.add("Tidak Tahu");
//            options.add("Cukup Yakin");
//            options.add("Yakin");
//            options.add("Sangat Yakin");
//
//            ArrayAdapter adapter = new ArrayAdapter(itemView.getContext(),
//                    android.R.layout.simple_spinner_dropdown_item, options);
//
//            spinnerText.setAdapter(adapter);
//
//            if (selectedspinners.get(pos)!=null&&selectedspinners.get(pos)!=-1){
//                spinnerText.setSelection(selectedspinners.get(pos));
//            }
//
//            spinnerText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    selectedspinners.put(pos,position);
//                    if ("Tidak".equals(options.get(position))) {
//                        lsItem.setCfuser(0.2);
//                    } else if ("Tidak Tahu".equals(options.get(position))) {
//                        lsItem.setCfuser(0.4);
//                    } else if ("Cukup Yakin".equals(options.get(position))) {
//                        lsItem.setCfuser(0.6);
//                    } else if ("Yakin".equals(options.get(position))) {
//                        lsItem.setCfuser(0.8);
//                    } else if ("Sangat Yakin".equals(options.get(position))) {
//                        lsItem.setCfuser(1);
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//        }
//    }
//}
//
