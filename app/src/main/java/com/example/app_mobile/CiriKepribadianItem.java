package com.example.app_mobile;

import java.io.Serializable;

public class CiriKepribadianItem implements Serializable {
    private String kode_kepribadian;
    private String ciri_kepribadian;
    private double cfuser;

    public CiriKepribadianItem() {

    }

    public CiriKepribadianItem(String kode_kepribadian, String ciri_kepribadian, double cfuser) {
        this.kode_kepribadian = kode_kepribadian;
        this.ciri_kepribadian = ciri_kepribadian;
        this.cfuser = cfuser;
    }

    public String getKode_kepribadian() {
        return kode_kepribadian;
    }

    public void setKode_kepribadian(String kode_kepribadian) {
        this.kode_kepribadian = kode_kepribadian;
    }

    public String getCiri_kepribadian() {
        return ciri_kepribadian;
    }

    public void setCiri_kepribadian(String ciri_kepribadian) {
        this.ciri_kepribadian = ciri_kepribadian;
    }

    public double getCfuser() {
        return cfuser;
    }

    public void setCfuser(double cfuser) {
        this.cfuser = cfuser;
    }

    @Override
    public String toString() {
        return "CiriKepribadianItem{" +
                "kode_kepribadian='" + kode_kepribadian + '\'' +
                ", ciri_kepribadian='" + ciri_kepribadian + '\'' +
                ", cfuser=" + cfuser +
                '}';
    }
}
