package com.example.app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class HasilKepribadian extends AppCompatActivity {
    public static final String KEPRIBADIAN = "kepribadian";
    public static final String PRESENTASE_TERBESAR = "presentase_terbesar";
    public static final String PRESENTASE_SANGUINIS = "presentase_sanguinis";
    public static final String PRESENTASE_MELANKOLIS = "presentase_melankolis";
    public static final String PRESENTASE_KORELIS = "presentase_korelis";
    public static final String PRESENTASE_PLEGMATIS = "presentase_plegmatis";
    public static final String NAME = "name";

    String name;
    Button btn_gayabelajar;
    TextView txt_diagnosa, txt_presentase, txt_deskripsi, txt_saran;
    double sanguinis, melankolis, korelis, plegmatis;
    double max_presentase;
    int index_kepribadian = 0;

    String diagnosa[] = {
            "SANGUINIS",
            "MELANKOLIS",
            "KOLERIS",
            "PLEGMATIS"
    };
    String deskripsi[] = {
            "Anak sanguinis dikenal sebagai pribadi yang ramah, energik, ceria, dan suka berbicara. Anak-anak ini memiliki banyak ide dan senang untuk berbicara menyampaikannya entah itu hal yang serius, atau sekedar mencairkan suasana.",
            "Melankolis memiliki unsur kepribadian yang basah. Melankolis dikenal memiliki pembawaan yang serius dan tekun sehingga dalam membuat perencanaan akan dilakukan secara detail. Selain itu, melankolis juga tipe yang mencintai keluarga dan berjiwa sosial. Sifat buruk melankolis yang terlalu sensitif membuatnya mudah khawatir dengan apa yang akan terjadi pada hidup. Kepribadian melankolis juga cenderung susah untuk percaya dengan orang lain.",
            "Koleris memiliki unsur kepribadian yang kering. Koleris dikenal memiliki pembawaan emosional, logis dan keras kepala. Biasanya pribadi seperti ini banyak dipilih menjadi pemimpin. Hal ini karena koleris memiliki kepribadian yang keras kepala dan logis sehingga bisa mengambil keputusan dengan tepat. Selain itu, koleris memiliki kepribadian yang fokus dan mandiri serta berani mengambil risiko dalam keputusan yang mereka buat.",
            "Plegmatis memiliki unsur kepribadian yang dingin. Plegmatis memiliki pembawaan yang santai dan cinta damai. Biasanya pribadi seperti ini memiliki hubungan yang baik dengan keluarga termasuk tetangga. Selain itu kepribadian plegmatis dikenal sebagai seorang yang sabar dan lembut sehingga sangat baik jika ikut dalam acara amal atau bakti sosial."
    };

    String saran[] = {
            "Anak-anak ini membutuhkan bimbingan dari orangtua, khususnya dalam hal mengatur perasaan mereka juga berpikir sebelum bertindak.",
            "Berikan pujian kepada anak melankolis soal keunggulannya, misalnya ketelitian dan perhatian terhadap detail yang sangat bagus. Penting juga untuk mengajari kesedihan dan menghadapi masalah, tak ketinggalan pula seni memaafkan. Karena kecenderungan anak-anak melankolis adalah menyimpan dendam, Orang Tua perlu membantu mereka untuk belajar memaafkan orang yang menyakiti hati mereka.",
            "Koleris memiliki unsur kepribadian yang kering. Koleris dikenal memiliki pembawaan emosional, logis dan keras kepala. Biasanya pribadi seperti ini banyak dipilih menjadi pemimpin. Hal ini karena koleris memiliki kepribadian yang keras kepala dan logis sehingga bisa mengambil keputusan dengan tepat. Selain itu, koleris memiliki kepribadian yang fokus dan mandiri serta berani mengambil risiko dalam keputusan yang mereka buat.",
            "Plegmatis memiliki unsur kepribadian yang dingin. Plegmatis memiliki pembawaan yang santai dan cinta damai. Biasanya pribadi seperti ini memiliki hubungan yang baik dengan keluarga termasuk tetangga. Selain itu kepribadian plegmatis dikenal sebagai seorang yang sabar dan lembut sehingga sangat baik jika ikut dalam acara amal atau bakti sosial."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_kepribadian);

        sanguinis = getIntent().getDoubleExtra(PRESENTASE_SANGUINIS, 0.0);
        melankolis = getIntent().getDoubleExtra(PRESENTASE_MELANKOLIS, 0.0);
        korelis = getIntent().getDoubleExtra(PRESENTASE_KORELIS, 0.0);
        plegmatis = getIntent().getDoubleExtra(PRESENTASE_PLEGMATIS, 0.0);
        name = getIntent().getStringExtra(NAME);

        btn_gayabelajar = (Button) findViewById(R.id.btn_gayabelajar);
        txt_diagnosa = findViewById(R.id.diagnosa);
        txt_presentase = findViewById(R.id.presentase);
        txt_deskripsi = findViewById(R.id.deskripsi);
        txt_saran = findViewById(R.id.saran);

        max_presentase = sanguinis;
        if (max_presentase < melankolis) {
            max_presentase = melankolis;
            index_kepribadian = 1;
        }
        if (max_presentase < korelis) {
            max_presentase = korelis;
            index_kepribadian = 2;
        }
        if (max_presentase < plegmatis) {
            max_presentase = plegmatis;
            index_kepribadian = 3;
        }

        txt_diagnosa.setText(diagnosa[index_kepribadian]);
        txt_presentase.setText(String.format("%.2f", max_presentase));
        txt_deskripsi.setText(deskripsi[index_kepribadian]);
        txt_saran.setText(saran[index_kepribadian]);

        btn_gayabelajar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HasilKepribadian.this, DiagnosaGayaBelajar.class);
                intent.putExtra(PRESENTASE_TERBESAR, max_presentase);
                intent.putExtra(KEPRIBADIAN, diagnosa[index_kepribadian]);
                intent.putExtra(NAME, name);
                startActivity(intent);
            }
        });
    }
}