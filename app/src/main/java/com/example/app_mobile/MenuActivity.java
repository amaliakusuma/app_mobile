package com.example.app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.card.MaterialCardView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MenuActivity extends AppCompatActivity {
    Button button_kepribadian, button_history;
    TextView txtName;
    MaterialCardView test;
    MaterialCardView test2;
    private ImageView actionLogout;
    private FirebaseAuth auth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        button_kepribadian = (Button) findViewById(R.id.button_kepribadian);
        button_history = findViewById(R.id.button_history);
        test = (MaterialCardView) findViewById(R.id.info_gb);
        test2 = (MaterialCardView) findViewById(R.id.info_personality);
        actionLogout = findViewById(R.id.imageLogout);
        txtName = findViewById(R.id.textUsername);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        txtName.setText(user.getDisplayName());

        actionLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                Intent intent = new Intent(MenuActivity.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        button_kepribadian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, DiagnosaKepribadian.class);
                startActivity(intent);
            }
        });

        button_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, HistoryDiagnosa.class);
                startActivity(intent);
            }
        });

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, Info_Learningstyle.class);
                startActivity(intent);
            }
        });

        test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, Info_Personality.class);
                startActivity(intent);
            }
        });
    }
}