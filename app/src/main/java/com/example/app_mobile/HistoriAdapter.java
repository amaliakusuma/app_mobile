package com.example.app_mobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

public class HistoriAdapter extends RecyclerView.Adapter<HistoriAdapter.ViewHolder> {
    private ArrayList<HasilDiagnosa> daftarHasilDiagnosa;

    public HistoriAdapter(ArrayList<HasilDiagnosa> daftarHasilDiagnosa) {
        this.daftarHasilDiagnosa = daftarHasilDiagnosa;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HasilDiagnosa diagnosa = daftarHasilDiagnosa.get(position);
        holder.txtNama.setText(diagnosa.getNama());
        holder.txtTanggal.setText(diagnosa.getTanggalDiagnosa().toDate().toString());
        holder.txtKepribadian.setText(String.format(Locale.getDefault(), "%s (%.2f%%)", diagnosa.getKepribadian(), diagnosa.getPersentaseKepribadian()));
        holder.txtGayaBelajar.setText(String.format(Locale.getDefault(), "%s (%.2f%%)", diagnosa.getGayaBelajar(), diagnosa.getPersentaseGayaBelajar()));
    }

    @Override
    public int getItemCount() {
        return daftarHasilDiagnosa.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtTanggal, txtKepribadian, txtGayaBelajar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtKepribadian = itemView.findViewById(R.id.txtKepribadian);
            txtGayaBelajar = itemView.findViewById(R.id.txtGayaBelajar);
        }
    }
}
