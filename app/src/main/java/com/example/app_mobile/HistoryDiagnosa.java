package com.example.app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class HistoryDiagnosa extends AppCompatActivity {
    //firebase
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser user;

    private ArrayList<HasilDiagnosa> daftarHasilDiagnosa;
    private RecyclerView recyclerView;
    private HistoriAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_diagnosa);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        recyclerView = findViewById(R.id.recyclerView_riwayat);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryDiagnosa.this, MenuActivity.class);
                startActivity(intent);
            }
        });

        // firebase
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        db = FirebaseFirestore.getInstance();

        // untuk mengambil data diagnosa dari firebase
        daftarHasilDiagnosa = new ArrayList<>();
        db.collection("diagnosa").whereEqualTo("userId", user.getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            HasilDiagnosa diagnosa = document.toObject(HasilDiagnosa.class);
                            daftarHasilDiagnosa.add(diagnosa);

                            adapter = new HistoriAdapter(daftarHasilDiagnosa);
                            recyclerView.setLayoutManager(new LinearLayoutManager(HistoryDiagnosa.this));
                            recyclerView.setAdapter(adapter);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(HistoryDiagnosa.this, "Gagal: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}